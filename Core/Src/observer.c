/*
 * observer.c
 *
 *  Created on: May 19, 2023
 *      Author: polecki
 */

#include "observer.h"

observer_s observer =
{
		.callback_array = {0},
		.callback_cnt = 0
};

void observer_subscribe(measure_callback callback)
{

	assert(callback != NULL);

	//check also if this is first subscription

	if (observer.callback_cnt < TOTAL_CALLBACKS)
	{
		observer.callback_array[observer.callback_cnt] = callback;
		observer.callback_cnt++;
	}
}

void observer_unsubscribe(measure_callback callback)
{
	assert(callback != NULL);

	for(int i = 0; i < observer.callback_cnt; i++)
	{
		if (observer.callback_array[i] == callback)
		{
			observer.callback_array[i] = NULL;
		}
	}

	//delete empty spaces in array when unsubscribed
}

void observer_run(void * data_pnt)
{
	for(int i = 0; i < observer.callback_cnt; i++)
	{
		if (NULL != observer.callback_array[i])
		{
			observer.callback_array[i](data_pnt);
		}
	}
}

