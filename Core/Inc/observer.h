/*
 * observer.h
 *
 *  Created on: May 19, 2023
 *      Author: polecki
 */

#ifndef INC_OBSERVER_H_
#define INC_OBSERVER_H_

#include <stdio.h>

enum
{
	FIRST_CALLBACK,
	SECOND_CALLBACK,
	THIRD_CALLBACK,
	TOTAL_CALLBACKS
};

typedef void (*measure_callback)(void*);

typedef struct
{
	measure_callback callback_array[TOTAL_CALLBACKS];

	uint32_t callback_cnt;

}observer_s;

void observer_subscribe(measure_callback callback);
void observer_unsubscribe(measure_callback callback);
void observer_run(void * data_pnt);

#endif /* INC_OBSERVER_H_ */
