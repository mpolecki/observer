# Wzorzec Adapter

## 1. Zastosowanie
Wzorzec używa się gdy w momencie powstania pewnego zdarzenia chemy wywołać pracę obiektów, które są tym zdarzeniem zainteresowane(subskrybent - klienci). Pozwala nam on uniknąć sytuacji, że każdy z tych obiektów odpytuje czasowo obiekt główny czy dane zdarzenie już nastąpiło.

## 2. Przykład użycia
Odczytujemy z czujnika pewne dane. Chcemy, aby w związku ze zmianą pewne wartości zostały na nowo przeliczone, zmieniła się jakaś logika, a także wyświetlić ostatni wynik.

- Należy stworzyć strukturę, która będzie przechowywać tablicę funkcji, które mają być wywołane. Argumentem takiej funkcji powinien być wskaźnik na najnowsze dane typu void, aby móc pracować z różnymi rodzajami typów:

	```c
	typedef void (*measure_callback)(void*);

	typedef struct
	{
		measure_callback callback_array[TOTAL_CALLBACKS];

		uint32_t callback_cnt;

	}observer_s;
	```
- Gdy chcemy, aby jakiś nowy klient reagował na zdarzenie dodajemy go do puli subskybentów:

	```c
	void observer_subscribe(measure_callback callback)
	{

		assert(callback != NULL);

		//check also if this is first subscription

		if (observer.callback_cnt < TOTAL_CALLBACKS)
		{
			observer.callback_array[observer.callback_cnt] = callback;
			observer.callback_cnt++;
		}
	}
	```

- Gdy zdarzenie nadejdzie aktywowane są wszystkie funkcje, które został zasubskrybowane

	```c
	void observer_run(void * data_pnt)
	{
		for(int i = 0; i < observer.callback_cnt; i++)
		{
			if (NULL != observer.callback_array[i])
			{
				observer.callback_array[i](data_pnt);
			}
		}
	}
	```

## 3. Przebieg programu:
Dodajemy subskrybentów do obiektu obserwera, a następnie za pomocą przycisku symulujemy zajście zdarzenia i po kolei wywołujemy funkcje klientów:

```c
  observer_subscribe(calculate_some_result);
  observer_subscribe(change_some_logic);
  observer_subscribe(display_some_value);

  printf("\e[1;1H\e[2J");
  printf("Observer patern example started!\r\n");

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

	  if (interrupt_received)
	  {
		  interrupt_received = false;
		  observer_run((void*)&test_data);
	  }

  }
```

